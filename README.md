# Spherical Coordinates

Spherical Coordinates is part of the Vanilla Unity SDK.

Spherical Coordinates provides an alternative way to represent positions in Unity to the standard cartesian (x,y,z) coordinate system.

It includes the struct VectorS which features three components; latitude (x), longitude (y) and radius (z).

Using a VectorS will let you easily translate an object rotationally around a point in world space. However, Unitys Transform component still works exclusively with cartesian positions, so you'll need to use .ToCartesian when you're ready to apply a VectorS position to a Transform.

Many of the learnings are based on the original implementation by Morten Nobel found here: https://blog.nobel-joergensen.com/2010/10/22/spherical-coordinates-in-unity/

---

## Installation

Vanilla Plus packages are installed through Unity's Package Manager using a [scoped registry](https://docs.unity3d.com/Manual/upm-scoped.html). Open your Unity Project of choice and select:

> Edit menu > Project settings > Package Manager > Scoped Registries > Plus button

Then add:


	name:      Vanilla Plus
	url:       https://registry.npmjs.com
	Scopes:    vanilla.plus

---

## Usage

```csharp
using UnityEngine;

using Vanilla.SphericalCoordinates;

public class RotateAroundTarget : MonoBehaviour
{

	public VectorS position = new VectorS(Radius: 3.0f,
	                                      Latitude: 0.0f,
	                                      Longitude: 0.0f);
	
	public Transform target;
	
	public float rotateSpeed = 0.5f;

	void Update() 
	{
		position.latitude += Time.deltaTime * rotateSpeed;
		
		transform.position = target.position + position.ToCartesian();
		
		transform.LookAt(target);
	}

}
```

---

## Contributing
Please don't. I have no idea what a pull request is and at this point I'm too afraid to ask.

If you hated this package, [let me know](mailto:lucas@vanilla.plus).

---

## Author

Lucas Hehir

---

## License
[The Unlicense](https://unlicense.org/)