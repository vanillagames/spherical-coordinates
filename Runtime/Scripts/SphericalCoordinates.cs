using UnityEngine;

namespace Vanilla.SphericalCoordinates
{

	public static class Extensions
	{
		
		// Transform

		public static VectorS GetSphericalPosition(this Transform input) => new VectorS(cartesianCoordinate: input.position);

		public static void SetSphericalPosition(this Transform input, VectorS position) => input.position = position.ToCartesian();

		public static void SetSphericalPosition(this Transform input, VectorS position, Transform relative) => input.position = relative.position + position.ToCartesian();

	}

}