﻿using System;

using UnityEngine;
 
namespace Vanilla.SphericalCoordinates
{
    
    // It's dangerous to go alone. Take this.

    // latitude and longitude work based on PI, with PI itself representing 180 degrees.
    // In order to more easily work with degrees, some conversion functions have been included
    // as well as the following pre-calculated chart.
    
    // 6.283186 - 360˚
    // 3.141593 - 180˚
    // 1.570796 - 90˚
    // 1.047198 - 60˚
    // 0.785398 - 45˚
    // 0.5235988 - 30˚
    // 0.2617967 - 15˚
    
    [Serializable]
    public struct VectorS
    {
        
        // --------------------------------------------------------------------------------------------------------------------------------- Data //

        [SerializeField]
        public float latitude;

        [SerializeField]
        public float longitude;

        [SerializeField]
        public float radius;

        // ------------------------------------------------------------------------------------------------------------------------- Constructors //


        public VectorS(float Latitude  = 0.0f,
                       float Longitude = 0.0f,
                       float Radius    = 0.0f)
        {
            latitude  = Latitude;
            longitude = Longitude;
            radius    = Radius;
        }


        public VectorS(VectorS copyFrom)
        {
            latitude  = copyFrom.latitude;
            longitude = copyFrom.longitude;
            radius    = copyFrom.radius;
        }


        public VectorS(Transform T) : this(cartesianCoordinate: T.position) { }


        public VectorS(Vector3 cartesianCoordinate = default)
        {
            if (cartesianCoordinate.x == 0f) cartesianCoordinate.x = Mathf.Epsilon;

            radius = cartesianCoordinate.magnitude;

            latitude = Mathf.Atan(f: cartesianCoordinate.z / cartesianCoordinate.x);

            if (cartesianCoordinate.x < 0f) latitude += Mathf.PI;

            longitude = Mathf.Asin(f: cartesianCoordinate.y / radius);
        }

        // -------------------------------------------------------------------------------------------------------------------------- Conversions //


        public Vector3 ToCartesian()
        {
            var a = radius * Mathf.Cos(f: longitude);

            return new Vector3(x: a      * Mathf.Cos(f: latitude),
                               y: radius * Mathf.Sin(f: longitude),
                               z: a      * Mathf.Sin(f: latitude));
        }


        public void FromCartesian(Vector3 cartesianCoordinate)
        {
            if (cartesianCoordinate.x == 0f) cartesianCoordinate.x = Mathf.Epsilon;

            radius = cartesianCoordinate.magnitude;

            latitude = Mathf.Atan(f: cartesianCoordinate.z / cartesianCoordinate.x);

            if (cartesianCoordinate.x < 0f) latitude += Mathf.PI;

            longitude = Mathf.Asin(f: cartesianCoordinate.y / radius);
        }

        // -------------------------------------------------------------------------------------------------------------------------------- Wraps //

        // Latitude


        public void WrapLatitudeByDegrees(float degrees) =>
            latitude = Mathf.Repeat(t: latitude,
                                    length: degrees * Mathf.Deg2Rad);


        public void WrapLatitudeByDegrees(float minDegrees,
                                          float maxDegrees)
        {
            var min = minDegrees * Mathf.Deg2Rad;

            latitude = min +
                       Mathf.Repeat(t: latitude - min,
                                    length: (maxDegrees - minDegrees) * Mathf.Deg2Rad);
        }


        public void WrapLatitudeByRadians(float radians) =>
            latitude = Mathf.Repeat(t: latitude,
                                    length: radians);


        public void WrapLatitudeByRadians(float minRadians,
                                          float maxRadians) =>
            latitude = minRadians +
                       Mathf.Repeat(t: latitude        - minRadians,
                                    length: maxRadians - minRadians);


        // Longitude


        public void WrapLongitudeByDegrees(float degrees) =>
            longitude = Mathf.Repeat(t: longitude,
                                     length: degrees * Mathf.Deg2Rad);


        public void WrapLongitudeByDegrees(float minDegrees,
                                           float maxDegrees)
        {
            var min = minDegrees * Mathf.Deg2Rad;

            longitude = min +
                        Mathf.Repeat(t: longitude - min,
                                     length: (maxDegrees - minDegrees) * Mathf.Deg2Rad);
        }


        public void WrapLongitudeByRadians(float radians) =>
            longitude = Mathf.Repeat(t: longitude,
                                     length: radians);


        public void WrapLongitudeByRadians(float minRadians,
                                           float maxRadians) =>
            longitude = minRadians +
                        Mathf.Repeat(t: longitude       - minRadians,
                                     length: maxRadians - minRadians);


        // ------------------------------------------------------------------------------------------------------------------------------- Clamps //

        // Latitude

        public void ClampLatitudeByDegrees(float degrees)
        {
            var radians = degrees * Mathf.Deg2Rad * 0.5f;

            latitude = Mathf.Clamp(value: latitude,
                                   min: -radians,
                                   max: radians);
        }


        public void ClampLatitudeByDegrees(float minDegrees,
                                           float maxDegrees) =>
            latitude = Mathf.Clamp(value: latitude,
                                   min: minDegrees * Mathf.Deg2Rad,
                                   max: maxDegrees * Mathf.Deg2Rad);


        public void ClampLatitudeByRadians(float radians) =>
            latitude = Mathf.Clamp(value: latitude,
                                   min: -radians * 0.5f,
                                   max: radians  * 0.5f);


        public void ClampLatitudeByRadians(float minRadians,
                                           float maxRadians) =>
            latitude = Mathf.Clamp(value: latitude,
                                   min: minRadians,
                                   max: maxRadians);


        // Longitude


        public void ClampLongitudeByDegrees(float degrees)
        {
            var radians = degrees * Mathf.Deg2Rad * 0.5f;

            longitude = Mathf.Clamp(value: longitude,
                                    min: -radians,
                                    max: radians);
        }


        public void ClampLongitudeByDegrees(float minDegrees,
                                            float maxDegrees) =>
            longitude = Mathf.Clamp(value: longitude,
                                    min: minDegrees * Mathf.Deg2Rad,
                                    max: maxDegrees * Mathf.Deg2Rad);


        public void ClampLongitudeByRadians(float radians) =>
            longitude = Mathf.Clamp(value: longitude,
                                    min: -radians * 0.5f,
                                    max: radians  * 0.5f);


        public void ClampLongitudeByRadians(float minRadians,
                                            float maxRadians) =>
            longitude = Mathf.Clamp(value: longitude,
                                    min: minRadians,
                                    max: maxRadians);

        
        // Radius


        public void ClampRadius(float maxMetres) =>
            radius = Mathf.Clamp(value: radius,
                                 min: 0.025f,
                                 max: maxMetres);


        public void ClampRadius(float minMetres,
                                float maxMetres) =>
            radius = Mathf.Clamp(value: radius,
                                 min: minMetres,
                                 max: maxMetres);

        // ------------------------------------------------------------------------------------------------------------------------------ Strings //

        public override string ToString() => $"[Latitude] {latitude} [Longitude] {longitude} [Radius] {radius}";

    }

}