﻿using System;

using UnityEngine;

namespace Vanilla.SphericalCoordinates.Samples 
{
	
	public class OrbitTransform : MonoBehaviour
	{

		[Header(header: "Target")] 
		[SerializeField]
		public Transform target;

		[SerializeField]
		public VectorS destination = new VectorS(Latitude: 0,
												 Longitude: 0,
												 Radius: 3.0f);

		[Header(header: "Input")] [SerializeField]
		public float rotateSpeed = 10f;
		[SerializeField]
		public float scrollSpeed = 1000f;

		[Header("Clamp Latitude")]
		[SerializeField]
		public bool clampLatitude = false;
		
		[SerializeField]
		public float latMin = -180.0f;
		[SerializeField]
		public float latMax = 0.0f;
		
		[Header("Clamp Longitude")]
		[SerializeField]
		public bool clampLongitude = true;

		[SerializeField]
		public float longMin = -45.0f;
		[SerializeField]
		public float longMax = 45.0f;

		[Header(header: "Clamp Radius")] 
		[SerializeField]
		public bool clampRadius = true;
		
		[SerializeField]
		public float radiusMin = 3.0f;
		[SerializeField]
		public float radiusMax = 10.0f;


		[Header(header: "Smoothing")] [SerializeField]
		public bool doSmoothing = true;

		[SerializeField]
		public float smoothTime = 0.0666f;

		[NonSerialized]
		public float smoothLatVelocity;

		[NonSerialized]
		public float smoothLongVelocity;

		[NonSerialized]
		public float smoothRadiusVelocity;

		[NonSerialized]
		public VectorS delta;


		private void Start()
		{
			destination.FromCartesian(cartesianCoordinate: transform.position);
			
			delta = destination;
		}


		void Update()
		{
			var p = target.position;
			
			// Input

			if (Input.GetMouseButton(button: 1))
			{
				destination.latitude   -= Input.GetAxis(axisName: "Mouse X") * rotateSpeed * Time.deltaTime;
				destination.longitude -= Input.GetAxis(axisName: "Mouse Y") * rotateSpeed * Time.deltaTime;
			}

			destination.radius -= Input.GetAxis(axisName: "Mouse ScrollWheel") * scrollSpeed * Time.deltaTime;
			
			// Clamp
			
			if (clampLatitude)
			{
				destination.ClampLatitudeByDegrees(minDegrees: latMin,
												maxDegrees: latMax);
			}

			if (clampLongitude)
			{
				destination.ClampLongitudeByDegrees(minDegrees: longMin,
												  maxDegrees: longMax);
			}
			
			if (clampRadius)
			{
				destination.ClampRadius(minMetres: radiusMin,
										maxMetres: radiusMax);
			}

			// Smooth
			
			if (doSmoothing)
			{

				delta.latitude = Mathf.SmoothDamp(current: delta.latitude,
											   target: destination.latitude,
											   currentVelocity: ref smoothLatVelocity,
											   smoothTime: smoothTime);

				delta.longitude = Mathf.SmoothDamp(current: delta.longitude,
												 target: destination.longitude,
												 currentVelocity: ref smoothLongVelocity,
												 smoothTime: smoothTime);
												 
				delta.radius = Mathf.SmoothDamp(current: delta.radius,
												target: destination.radius,
												currentVelocity: ref smoothRadiusVelocity,
												smoothTime: smoothTime);

			}

			// Apply

			ApplyPosition();

			transform.LookAt(worldPosition: p);
		}

		private void ApplyPosition() => transform.position = target.position + (doSmoothing ? delta.ToCartesian() : destination.ToCartesian());

	}
	
}